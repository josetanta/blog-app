"""Testing of users and model"""

# Flask
from flask.testing import FlaskClient

# Seeders
from app.seeders import fake


def it_register_users_register_success(client: FlaskClient):

    data = {
        'username': fake.user_name(),
        'email': fake.email(),
        'password': 'fake-user',
        'password_repeat': 'fake-user'
    }

    response = client.post(
        '/api/users/',
        data=data,
        content_type='multipart/form-data',
    )

    assert response.status_code == 200
