import unittest
import json
import time

from app import create_app, db
from app.models import User
from app.seeders import create_roles, fake


class AuthTestCase(unittest.TestCase):
    """
    Settings Authentication Test Case
    for unit testing
    """

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        create_roles()
        self.client = self.app.test_client()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def api_headers(self, token=''):
        headers = {
            'Accept': 'application/json; multipart/form-data; application/x-www-form-urlencoded',
            'Content-Type': 'application/json; multipart/form-data; application/x-www-form-urlencoded',
        }

        if bool(token):
            headers['Authorization'] = f'Bearer {token}'

        return headers

    def test_should_register_user_success(self):

        data = {
            'username': fake.user_name(),
            'email': fake.email(),
            'password': 'fake-user',
            'password_repeat': 'fake-user'
        }

        response = self.client.post(
            '/api/users/',
            data=data,
            content_type='multipart/form-data',
        )
        self.assertEqual(response.status_code, 200)

        data_json = json.loads(response.get_data(as_text=True))
        self.assertEqual(data_json['data']['username'], data['username'])

        response = self.client.get(
            '/api/refresh-token/',
            headers=self.api_headers(data_json['token'])
        )
        self.assertEqual(response.status_code, 403)

    def test_should_signin_success(self):

        data = {
            'username': fake.user_name(),
            'email': fake.email(),
            'password': 'fake-user',
            'password_repeat': 'fake-user'
        }

        response = self.client.post(
            '/api/users/',
            data=data,
            content_type='multipart/form-data',
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.post(
            '/api/login/',
            data={'email': data['email'], 'password': data['password']}
        )
        self.assertEqual(response.status_code, 200)

        data_json = json.loads(response.get_data(as_text=True))
        self.assertIsNotNone(data_json['token'])

    def test_should_signin_error(self):

        data = {
            'username': fake.user_name(),
            'email': fake.email(),
            'password': 'fake-user',
            'password_repeat': 'fake-user'
        }

        response = self.client.post(
            '/api/users/',
            data=data,
            content_type='multipart/form-data',
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.post(
            '/api/login/',
            data={'email': data['email'], 'password': 'other-password'}
        )
        self.assertEqual(response.status_code, 400)

    def test_should_refresh_token_ok(self):

        user = User(
            username=fake.user_name(),
            email=fake.email(),
            password='fake-user',
            uuid=fake.uuid4(),
            confirmed=True
        )

        response = self.client.post(
            '/api/login/',
            data={'email': user.email, 'password': 'fake-user'},
            content_type='multipart/form-data',
        )
        self.assertEqual(response.status_code, 200)

        data_json = json.loads(response.get_data(as_text=True))
        self.assertIsNotNone(data_json['token'])

        token = data_json['token']
        response = self.client.get('/api/refresh-token/', headers=self.api_headers(token))
        self.assertEqual(response.status_code, 200)

        data_json = json.loads(response.get_data(as_text=True))
        self.assertEqual(data_json['message'], 'ok')
        self.assertIsNotNone(data_json['token'])

    def test_twice_login(self):

        user = User(
            username=fake.user_name(),
            email=fake.email(),
            password='fake-user',
            uuid=fake.uuid4(),
            confirmed=True
        )

        # Login 1
        response = self.client.post(
            '/api/login/',
            data={'email': user.email, 'password': 'fake-user'},
            content_type='multipart/form-data',
        )
        self.assertEqual(response.status_code, 200)
        data_json = json.loads(response.get_data(as_text=True))
        self.assertIsNotNone(data_json['token'])

        print('\nSleeping 😴')
        time.sleep(3)
        print('\nGoo! 🚀')

        # Login 2
        response = self.client.post(
            '/api/login/',
            data={'email': user.email, 'password': 'fake-user'},
            content_type='multipart/form-data',
        )
        self.assertEqual(response.status_code, 200)
        data_json = json.loads(response.get_data(as_text=True))
        self.assertIsNotNone(data_json['token'])

        response = self.client.post(
            '/api/logout/',
            headers=self.api_headers(data_json['token'])
        )
        self.assertEqual(response.status_code, 200)

    def test_should_unconfirmed_account(self):
        user1 = User(
            username=fake.user_name(),
            email=fake.email(),
            password='fake-user',
            uuid=fake.uuid4()
        )

        db.session.add(user1)
        db.session.commit()

        token_confirmed = user1.generate_confirmed_token(1)
        time.sleep(2)
        self.assertFalse(user1.confirm(token_confirmed))

    def test_should_unconfirmed_account_with_other_user(self):
        user1 = User(
            username=fake.user_name(),
            email=fake.email(),
            password='fake-user',
            uuid=fake.uuid4()
        )

        user2 = User(
            username=fake.user_name(),
            email=fake.email(),
            password='fake-user',
            uuid=fake.uuid4()
        )

        db.session.add_all([user1, user2])
        db.session.commit()

        token_confirmed_1 = user1.generate_confirmed_token(1)
        token_confirmed_2 = user2.generate_confirmed_token(1)

        self.assertFalse(user2.confirm(token_confirmed_1))
        self.assertFalse(user1.confirm(token_confirmed_2))
