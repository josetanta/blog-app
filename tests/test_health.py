from tests.utils import APITestSetup


class HealthTestCase(APITestSetup):

    WITH_USER = False

    def test_should_health_this_ok(self):

        response = self.client.get('/api/healthy/')

        self.assertEqual(response.status_code, 200)

        data_json = self.parse_data(response)

        self.assertEqual(data_json['status'], 'ok')
