import os
import secrets
from datetime import datetime, timedelta

from PIL import Image
from flask import current_app, url_for
from markdown.extensions.toc import slugify


def save_upload(img, directory='', obj=None, size=None, percent=1.0):
    # Traer el path del upload Post

    if obj:
        img_deleted = os \
            .path \
            .join(current_app.root_path, f"static/uploads/{directory}", str(obj.image))

        if os.path.exists(img_deleted):
            # Eliminar el upload actualz
            os.remove(img_deleted)

    hash_name_image = secrets.token_hex(12)

    _, b_img = os.path.splitext(img.filename)
    t_img = hash_name_image + b_img

    image_path = os.path.join(
        current_app.root_path,
        f'static/uploads/{directory}',
        t_img
    )

    save_img = Image.open(img)

    if isinstance(size, tuple):
        save_img.thumbnail(size)

    if isinstance(size, int):
        width, height = size * percent, size * percent
        save_img.thumbnail((width, height))
    else:
        w, h = save_img.size
        width, height = w * percent, h * percent
        save_img.thumbnail((width, height))

    save_img.save(image_path)

    return t_img


def delete_upload(obj, directory=''):
    image_current_del = os.path.join(
        current_app.root_path, f"static/uploads/{directory}", obj.image)

    if os.path.exists(image_current_del):
        # Eliminar el upload actual
        os.remove(image_current_del)


def _generate_slug(string) -> str:
    return slugify(string, '-')


def expire_token(token):
    time_elapsed = datetime.now() - token.date_created
    left_time = timedelta(
        days=current_app.config['EXPIRE_TOKEN']) - time_elapsed

    return left_time


def is_token_expired(token):
    return expire_token(token) < timedelta(seconds=0)


def format_urls(env_url: str):
    try:
        urls = env_url.split(',')
        convert_to_list = list(map(lambda x: x.strip(), urls))

        return convert_to_list
    except AttributeError:
        return []


def get_uuid():
    import uuid
    return str(uuid.uuid4())[:18] + f'{abs(datetime.today().__hash__())}'


def get_image_url(folder='', image=''):
    is_debug = bool(current_app.config['DEBUG'])
    is_proxy = current_app.config['FLASK_PROXY']

    url = None

    if is_debug:
        url = url_for(
            'static', filename=f'uploads/{folder}/{image}', _external=False)
        return str(url)

    if not is_debug and is_proxy:
        url = url_for(
            'static', filename=f'uploads/{folder}/{image}', _external=False)
        return str(url)

    return str(url)


def get_url(endpoint='', **values):
    is_debug = bool(current_app.config['DEBUG'])
    is_proxy = current_app.config['FLASK_PROXY']

    if is_debug:
        url = url_for(endpoint, **values, _external=False)
        return str(url)

    if not is_debug and is_proxy:
        url = url_for(endpoint, **values, _external=False)
        return str(url)

    url = url_for(endpoint, **values, _external=True)
    return str(url)
