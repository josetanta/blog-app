import random

from .models import (
    db,
    Role,
    Post,
    User,
    Comment
)

# utilities
from faker import Faker
from string import digits, ascii_uppercase

fake = Faker()


def create_roles():
    """
    Generated fake data
    """
    Role.generate_roles()
    db.session.commit()


def generate_users(count=10):
    users = []
    while(count > 0):
        users.append(
            User(
                username=fake.user_name(),
                email=fake.email(),
                uuid=fake.uuid4(),
                confirmed=True,
                password='seeders'
            )
        )
        count -= 1

    db.session.add_all(users)
    db.session.commit()

    del users

    return User.query.all()


def generate_posts(count=10, count_users=3):
    users = generate_users(count_users)
    title = 'Post-Title'
    posts = []

    while(count > 0):
        user = random.choice(users)
        t = ascii_uppercase + digits
        p = random.choice(title.join(random.choices(t, k=5)))
        posts.append(
            Post(
                title=p,
                content=fake.text(),
                uuid=fake.uuid4(),
                author=user
            )
        )
        count -= 1

    db.session.add_all(posts)
    db.session.commit()

    del posts

    return Post.query.all()


def generate_comments(count=10, count_posts=4, count_users=2):
    posts = generate_posts(count_posts, count_users=count_users)

    comments = []

    while(count > 0):
        post = random.choice(posts)

        comments.append(
            Comment(
                content=fake.text(),
                author=post.author,
                post=post
            )
        )
        count -= 1

    db.session.add_all(comments)
    db.session.commit()

    del comments

    return Comment.query.all()
