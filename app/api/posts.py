from flask import (
    url_for,
    jsonify,
)
from flask.views import MethodView
from flask_httpauth import request, current_app
from werkzeug.datastructures import CombinedMultiDict
from werkzeug.exceptions import BadRequestKeyError

from app import db
from app.models import Post, Comment, Permission
from app.utils import save_upload, delete_upload, get_uuid
from app.api import api
from .authentication import token_auth
from .decorators import permission_required, form_valid_request
from .forms import PostFormAPI
from .handlers import bad_request, not_found, internal_server, get_ok, create_ok
from .utils import register_api


class PostAPIMethodView(MethodView):

    def get(self, post_id: str = None):
        if post_id:
            post = Post.query.get(post_id)
            if post:
                return get_ok('OK', data=post.to_json())
            else:
                return not_found('No se encontro la publicación', data=None)
        else:

            page = request.args.get('page', 1, type=int)
            pagination = Post.query \
                .order_by(Post.date_created.desc()) \
                .paginate(page, per_page=current_app.config['APP_PER_PAGE'], error_out=False)
            posts = pagination.items

            prev_page = None
            if pagination.has_prev:
                prev_page = url_for(
                    'api.post_api', page=page - 1, _external=True)
            next_page = None
            if pagination.has_next:
                next_page = url_for(
                    'api.post_api', page=page + 1, _external=True)

            data = {
                'data': [post.to_json() for post in posts],
                'links': {
                    'self': url_for('api.post_api', _external=True),
                    'next_page': next_page,
                    'prev_page': prev_page},
                'count': pagination.total,
                'pages': pagination.pages
            }

            return get_ok('Ok', **data)

    @token_auth.login_required
    @permission_required(Permission.WRITE_POST)
    @form_valid_request(PostFormAPI)
    def post(self):
        try:
            form = PostFormAPI(
                CombinedMultiDict((request.form, request.files))
            )
            if form.validate_on_submit():
                post = Post(
                    title=form.title.data,
                    content=form.content.data,
                    uuid=get_uuid()
                )

                if form.image.data:
                    img = save_upload(
                        form.image.data,
                        directory='posts',
                        obj=post,
                        size=1500
                    )

                    post.image = img

                post.author = token_auth.current_user()

                db.session.add(post)
                db.session.commit()

                return create_ok('La publicación ha sido creado', data=post.to_json())

            return bad_request('Error al crear la publicación')

        except BadRequestKeyError:
            return internal_server('Error de servidor')

    @token_auth.login_required
    @permission_required(Permission.WRITE_POST)
    def delete(self, post_id):

        post = Post.query.get(post_id)

        if post:
            delete_upload(post, directory='posts')
            db.session.delete(post)
            db.session.commit()
            return get_ok('La publicación fue elimnado')
        else:
            return not_found('La publicación no fue encontrado')

    @token_auth.login_required
    @permission_required(Permission.WRITE_POST)
    @form_valid_request(PostFormAPI)
    def put(self, post_id: int):
        try:
            form = PostFormAPI(CombinedMultiDict(
                (request.form, request.files)))

            post = Post.query.get(post_id)

            if post and form.validate_on_submit():
                post.title = form.title.data
                post.content = form.content.data

                try:
                    if form.image.data:
                        img = save_upload(
                            form.image.data,
                            directory='posts',
                            obj=post
                        )
                        post.image = img
                    else:
                        pass
                except AttributeError:
                    pass

                db.session.commit()
                return get_ok('Su publicación fue actualizado', data=post.to_json())

            else:
                return bad_request('La publicación no fue encontrado')

        except BadRequestKeyError:
            return internal_server('Error de servidor')

    @token_auth.login_required
    @permission_required(Permission.WRITE_POST)
    @form_valid_request(PostFormAPI)
    def patch(self, post_id: int):
        try:
            form = PostFormAPI(CombinedMultiDict(
                (request.form, request.files)))

            post = Post.query.get(post_id)

            if post and form.validate_on_submit():

                post.title = form.title.data
                post.content = form.content.data

                try:
                    if form.image.data:
                        img = save_upload(
                            form.image.data,
                            directory='posts',
                            obj=post,
                            size=700
                        )

                        post.image = img
                    else:
                        pass
                except AttributeError:
                    pass

                db.session.commit()
                return get_ok('La publicación fue actualizado', data=post.to_json())

            else:

                return bad_request('La publicación no fue encontrado')
        except BadRequestKeyError:
            return internal_server('Error de servidor')


register_api(PostAPIMethodView, 'post_api',
             '/posts/', pk='post_id', pk_type='int')


@api.get('/posts/<uuid>/')
def post_uuid(uuid):
    post = Post.query.filter_by(uuid=uuid).first()
    if post:
        return get_ok('OK', data=post.to_json())
    else:
        return not_found('No se encontro la publicación')


@api.get('/posts/<int:post_id>/comments/')
def post_comments(post_id):
    page = request.args.get('page', 1, type=int)
    pagination = Comment.query \
        .filter_by(post_id=post_id) \
        .order_by(Comment.date_created.desc()) \
        .paginate(page, per_page=current_app.config['APP_PER_PAGE'], error_out=False)

    comments = pagination.items

    prev_page = None
    if prev_page:
        prev_page = url_for('api.post_comments',
                            post_id=post_id, page=page - 1, _external=True)

    next_page = None
    if next_page:
        next_page = url_for('api.post_comments',
                            post_id=post_id,
                            page=page + 1,
                            _external=True
                            )

    data = {
        'data': [comment.to_json() for comment in comments],
        'links': {'next_page': next_page, 'prev_page': prev_page},
        'count': pagination.total
    }

    return get_ok("Ok", **data)


@api.get('/posts/<uuid>/comments/')
def post_comments_uuid(uuid):
    page = request.args.get('page', 1, type=int)
    post = Post.query.filter_by(uuid=uuid).first()
    pagination = Comment.query \
        .filter_by(post_id=post.id) \
        .order_by(Comment.date_created.desc()) \
        .paginate(page, per_page=current_app.config['APP_PER_PAGE'], error_out=False)

    comments = pagination.items

    prev_page = None
    if prev_page:
        prev_page = url_for('api.post_comments_slug',
                            uuid=uuid, page=page - 1, _external=True)

    next_page = None
    if next_page:
        next_page = url_for('api.post_comments_slug',
                            uuid=uuid, page=page + 1, _external=True)

    data = {
        'data': [comment.to_json() for comment in comments],
        'links': {'next_page': next_page, 'prev_page': prev_page},
        'count': pagination.total
    }
    return get_ok('Ok', **data)


@api.get('/posts/<int:post_id>/comments/')
def post_comment(post_id: int):
    comment = Comment.query.filter_by(post_id=post_id).first()

    return jsonify({'data': comment.to_json()})
