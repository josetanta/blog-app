from flask import jsonify

from app.api import api
from .exceptions import ValidationError


def forbidden(message: str, **kwargs: dict[int, str]):
    return jsonify({'error': 'Forbidden', 'message': message, **kwargs}), 403


def not_found(message: str, **kwargs: dict[int, str]):
    return jsonify({'error': 'Not found', 'message': message, **kwargs}), 404


def unathorized(message: str, **kwargs: dict[int, str]):
    return jsonify({'error': 'Unathorized', 'message': message, **kwargs}), 401


def bad_request(message: str, **kwargs: dict[int, str]):
    return jsonify({'error': 'Bad request', 'message': message, **kwargs}), 400


def create_ok(message: str, **kwargs: dict[int, str]):
    return jsonify({'message': message, **kwargs}), 201


def get_ok(message: str, **kwargs: dict[int, str]):
    return jsonify({'message': message, **kwargs}), 200


def get_405(e):
    return e, 405


def internal_server(message: str, **kwargs: dict[int, str]):
    return jsonify({'error': 'Internal error', 'message': message, **kwargs}), 500


@api.errorhandler(ValidationError)
def validation_error(e):
    return bad_request(e.args[0])
